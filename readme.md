# Vaccination - API

## 💻 Sobre

Aplicação desenvolvida para armazenar e registrar cartões de vacinação, contendo dados como nome, cpf, data da primeira dose, data prevista para segunda dose, nome da vacina e a unidade de saúde.

## 🛠 Instalação

_# Este passo é para baixar o projeto_

**git clone https://gitlab.com/<your_user>/vaccination.git**

Entrar na pasta, criar um ambiente virtual e instalar as dependências:

_# Entrar na pasta_

**cd vaccination**

_# Criar um ambiente virtual_

**python3 -m venv venv**

_# Instalar as dependências_

**pip install -r requirements.txt**

Rodar as migrations para que o banco de dados e as tabelas sejam criadas:

_# Rodar as migrations_

**flask db migrate**

_# Rodar o servidor_

**flask run**

## 🚀 Utilização

Para utilizar este sistema, é necessário utilizar um API Client, como o Insomnia.

## Criação cartões de vacinação

**POST** /vaccination/

Rota para criação dos cartões de vacinação.

Exemplo do corpo da requisição:

```json
{
  "cpf": "01234567891",
  "name": "Chrystian",
  "vaccine_name": "Pfizer",
  "health_unit_name": "Santa Rita"
}
```

Exemplo de retorno:

RESPONSE STATUS -> HTTP 201 (Created)

```json
{
  "cpf": "01234567891",
  "name": "Chrystian",
  "first_shot_date": "Fri, 29 Oct 2021 16:36:13 GMT",
  "second_shot_date": "Thu, 27 Jan 2022 16:36:13 GMT",
  "vaccine_name": "Pfizer",
  "health_unit_name": "Santa Rita"
}
```

RESPONSE STATUS -> HTTP 400 (Bad Request)
Caso os campos sejam inválidos ou esteja faltando algum campo.

RESPONSE STATUS -> HTTP 409 (Conflict)
Caso haja a tentativa de criação de um usuário que já está cadastrado.

#

## Obter lista dos cartões de vacinação:

**GET** /vaccination/

Lista de cartões, mostrando as pessoas cadastradas no sistema.

RESPONSE STATUS -> HTTP 200 (ok)

```json
[
  {
    "cpf": "01234567891",
    "name": "Chrystian",
    "first_shot_date": "Fri, 29 Oct 2021 16:30:31 GMT",
    "second_shot_date": "Thu, 27 Jan 2022 16:30:31 GMT",
    "vaccine_name": "Pfizer",
    "health_unit_name": "Santa Rita"
  },
  {
    "cpf": "19876543210",
    "name": "Cauan",
    "first_shot_date": "Fri, 29 Oct 2021 16:31:30 GMT",
    "second_shot_date": "Thu, 27 Jan 2022 16:31:30 GMT",
    "vaccine_name": "Coronavac",
    "health_unit_name": "Santa Rita"
  },
  {
    "cpf": "54221194161",
    "name": "Eduardo",
    "first_shot_date": "Fri, 29 Oct 2021 16:35:24 GMT",
    "second_shot_date": "Thu, 27 Jan 2022 16:35:24 GMT",
    "vaccine_name": "Coronavac",
    "health_unit_name": "Santa Rita"
  }
]
```

---
