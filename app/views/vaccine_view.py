from app.exceptions.exceptions import WrongCpfError
from app.controllers.vaccine_controller import verify_cpf
from flask import Blueprint, request, current_app, jsonify
from app.models.vaccine_model import VaccineCard

bp = Blueprint("vaccination", __name__, url_prefix="/vaccination")

@bp.route('', methods=['POST'])
def create_card():
    data = request.get_json()
    person = VaccineCard(
        cpf=data["cpf"],
        name=data["name"],
        vaccine_name=data["vaccine_name"],
        health_unit_name=data["health_unit_name"]
    )
    try:
        verify_cpf(data)
        session = current_app.db.session
        session.add(person)
        session.commit()

        return jsonify(person)

    except WrongCpfError as e:
        return {'message': e.message}, 400


@bp.route('', methods=['GET'])
def get_cards():
    cards_list = VaccineCard.query.all()
    return jsonify(cards_list), 200