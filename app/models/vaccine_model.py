import datetime
from datetime import datetime, timedelta
from sqlalchemy import Column, String
from dataclasses import dataclass

from sqlalchemy.sql.sqltypes import DateTime

from app.configs.database import db

@dataclass
class VaccineCard(db.Model):
    cpf: str
    name: str
    first_shot_date: DateTime
    second_shot_date: DateTime
    vaccine_name: str
    health_unit_name: str

    __tablename__ = "vaccine_card"

    cpf = Column(String, primary_key=True)
    name = Column(String, nullable=False)
    first_shot_date = Column(DateTime, default=datetime.now())
    second_shot_date = Column(DateTime, default=(datetime.now() + timedelta(90)))
    vaccine_name = Column(String, nullable=False)
    health_unit_name = Column(String)


    


    