from app.exceptions.exceptions import WrongCpfError

def verify_cpf(data):
    person_cpf = data['cpf']

    if len(person_cpf) != 11 or person_cpf.isnumeric() == False:
        raise WrongCpfError()


    