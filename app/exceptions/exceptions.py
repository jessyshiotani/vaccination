class WrongCpfError(Exception):
    def __init__(self, message = {"error": "incorrect cpf field"}) -> None:
        self.message = message
        super().__init__(self.message)
    